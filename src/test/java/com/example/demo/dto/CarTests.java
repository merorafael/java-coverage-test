package com.example.demo.dto;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
class CarTests {
    @Test
    void testNumberOfWheels() {
        Car car = new Car();
        assertEquals(4, car.getNumberOfWheels());
    }
}
