package com.example.demo.dto;

public class Motorcycle extends Vehicle {
    @Override
    public Integer getNumberOfWheels() {
        return 2;
    }
}
