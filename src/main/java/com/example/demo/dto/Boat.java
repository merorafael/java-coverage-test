package com.example.demo.dto;

public class Boat extends Vehicle {
    @Override
    public Integer getNumberOfWheels() {
        return 0;
    }

    public Integer getNumberOfSails() {
        return 1;
    }
}
