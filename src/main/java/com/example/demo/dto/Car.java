package com.example.demo.dto;

public class Car extends Vehicle {
    private Boolean hasAC;

    public Boolean hasAC() {
        return hasAC;
    }

    public void setHasAC(Boolean hasAC) {
        this.hasAC = hasAC;
    }

    @Override
    public Integer getNumberOfWheels() {
        return 4;
    }
}
