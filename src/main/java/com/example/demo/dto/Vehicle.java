package com.example.demo.dto;

public abstract class Vehicle {
    protected String brand;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public abstract Integer getNumberOfWheels();
}
